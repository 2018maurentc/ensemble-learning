ABOUT THE PROJECT 

This project consists of implementing a Decision Tree from scratch in Python. We have developed two decision trees from scratch. One is dedicated to classification (DecisionTree_Classification) and the second to regression (DecisionTree_Regression). Our implementation uses the following libraries: numpy, pandas, random, math, time, sklearn.tree and sklearn.metrics. 

For both tasks, we start by initiating a DecisionBinaryTree class that basically builds the decision tree, helping to identify the nodes and to create new ones by splitting at a given feature and value. Then, we create two different classes, DecisionTree_Classification and DecisionTree_Regression that are used to build the models: fit to the data, predict the targets and compute the evaluation metrics. 

Here comes the main difference between our two implementations for the decision tree dedicated to regression and the one for classification. 
For classification, we calculate the gini index with the formula seen in class that will compute the purity of each split, or the cross entropy loss.

```math
Gini\ index = 1 -  \sum_{i=1}^n(P_{i}) ^2 
```

For regression, we calculate the mean square error that will be used at each split to compute the error. 
```math
MSE =  \frac{1}{n}  \sum_{i=1}^n( Y_{i} -  \widehat{Y}_{i})^{2}    
```
Then, we use those values to determine the best split (feature and value) at each node. 
For classification, a criterion can be chosen (Gini Index or Cross Entropy). The split with the lowest value of the criterion will be selected as the best split. 
For regression, the best split will be the one that has the least mean squared error. 

Our decision tree classes (for both regression and classification) benefit from the same structure as the sklearn implementation. The models take as parameters max_depth, max_features, min_samples_split, random_state. 
The training and predictions are similar in both trees, except for the splitting condition (Gini/Cross Entropy, MSE). The training runs until one of the breaking conditions is reached (max_features, max_depth, …).
The evaluation of the model is done by comparing the predicted value to the actual target. For classification, the accuracy score is calculated. For regression, we use the R2 score to evaluate the performance. 

Finally, we use existing datasets from the sklearn library (load_breast_cancer and load_boston) to test our implementations and compare their performances to the sklearn models DecisionTreeClassifier() and DecisionTreeRegressor().


